locals {
  _rds_clusters = {
    for rds_cluster in var.rds_clusters :
    rds_cluster.name => {
      urgent = merge(var.rds_clusters_default_thresholds.urgent, lookup(rds_cluster, "urgent", {}))
      casual = merge(var.rds_clusters_default_thresholds.casual, lookup(rds_cluster, "casual", {}))
    }
  }

  _rds_instances = {
    for rds_instance in var.rds_instances :
    rds_instance.name => {
      urgent = merge(var.rds_instances_default_thresholds.urgent, lookup(rds_instance, "urgent", {}))
      casual = merge(var.rds_instances_default_thresholds.casual, lookup(rds_instance, "casual", {}))
  } }
}

## RDS Cluster monitoring

resource "aws_cloudwatch_metric_alarm" "rds_cluster_writer_connections_urgent" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name          = "URG:${local.name_prefix}RDSCluster-Writer-Connections-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = each.value.urgent.writer_connections_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.writer_connections_evaluation_periods_count
  statistic           = "Sum"
  threshold           = each.value.urgent.writer_connections_count_threshold
  alarm_description   = "${each.key} RDS cluster writer maximum connection alarm"
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "WRITER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_writer_connections_casual" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name          = "CAS:${local.name_prefix}RDSCluster-Writer-Connections-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = each.value.casual.writer_connections_evaluation_period_seconds
  evaluation_periods  = each.value.casual.writer_connections_evaluation_periods_count
  statistic           = "Sum"
  threshold           = each.value.casual.writer_connections_count_threshold
  alarm_description   = "${each.key} RDS cluster writer maximum connection alarm"
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "WRITER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_reader_connections_urgent" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name          = "URG:${local.name_prefix}RDSCluster-Reader-Connections-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = each.value.urgent.reader_connections_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.reader_connections_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.urgent.reader_connections_count_threshold
  alarm_description   = "RDS Maximum connection Alarm for ${each.key} RDS cluster's reader's"
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "READER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_reader_connections_casual" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name          = "CAS:${local.name_prefix}RDSCluster-Reader-Connections-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = each.value.casual.reader_connections_evaluation_period_seconds
  evaluation_periods  = each.value.casual.reader_connections_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.casual.reader_connections_count_threshold
  alarm_description   = "RDS Maximum connection Alarm for ${each.key} RDS cluster's reader's"
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "READER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_writer_cpu_urgent" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name          = "URG:${local.name_prefix}RDSCluster-Writer-CPU-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = each.value.urgent.writer_cpu_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.writer_cpu_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.urgent.writer_cpu_threshold_percent
  alarm_description   = "${each.key} RDS cluster writer CPU alarm"
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data  = "ignore"
  datapoints_to_alarm = var.datapoints_to_alarm
  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "WRITER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_writer_cpu_casual" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name          = "CAS:${local.name_prefix}RDSCluster-Writer-CPU-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = each.value.casual.writer_cpu_evaluation_period_seconds
  evaluation_periods  = each.value.casual.writer_cpu_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.casual.writer_cpu_threshold_percent
  alarm_description   = "${each.key} RDS cluster writer CPU alarm"
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "WRITER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_reader_cpu_urgent" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name          = "URG:${local.name_prefix}RDSCluster-Reader-CPU-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = each.value.urgent.reader_cpu_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.reader_cpu_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.urgent.reader_cpu_threshold_percent
  alarm_description   = "${each.key} RDS cluster Reader CPU alarm"
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "READER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_reader_cpu_casual" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name          = "CAS:${local.name_prefix}RDSCluster-Reader-CPU-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = each.value.casual.reader_cpu_evaluation_period_seconds
  evaluation_periods  = each.value.casual.reader_cpu_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.casual.reader_cpu_threshold_percent
  alarm_description   = "${each.key} RDS cluster Reader CPU alarm"
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "READER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_replica_lag_urgent" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name          = "URG:${local.name_prefix}RDSCluster-Replica-Lag-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "AuroraReplicaLag"
  namespace           = "AWS/RDS"
  period              = each.value.urgent.replica_lag_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.replica_lag_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.urgent.replica_lag_threshold_millis
  alarm_description   = "${each.key} RDS cluster replication lag"
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "READER"
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cluster_replica_lag_casual" {
  for_each = { for alarms, alarm in local._rds_clusters : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name          = "CAS:${local.name_prefix}RDSCluster-Replica-Lag-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "AuroraReplicaLag"
  namespace           = "AWS/RDS"
  period              = each.value.casual.replica_lag_evaluation_period_seconds
  evaluation_periods  = each.value.casual.replica_lag_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.casual.replica_lag_threshold_millis
  alarm_description   = "${each.key} RDS cluster replication lag"
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBClusterIdentifier = each.key
    Role                = "READER"
  }
}


## RDS Instances monitoring

resource "aws_cloudwatch_metric_alarm" "rds_instance_connections_urgent" {
  for_each = { for alarms, alarm in local._rds_instances : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name          = "URG:${local.name_prefix}RDSInstance-Connections-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = each.value.urgent.connections_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.connections_evaluation_periods_count
  statistic           = "Sum"
  threshold           = each.value.urgent.connections_count_threshold
  alarm_description   = "${each.key} RDS instance writer maximum connection alarm"
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_instance_connections_casual" {
  for_each = { for alarms, alarm in local._rds_instances : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name          = "CAS:${local.name_prefix}RDSInstance-Connections-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = each.value.casual.connections_evaluation_period_seconds
  evaluation_periods  = each.value.casual.connections_evaluation_periods_count
  statistic           = "Sum"
  threshold           = each.value.casual.connections_count_threshold
  alarm_description   = "${each.key} RDS instance writer maximum connection alarm"
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_instance_cpu_urgent" {
  for_each = { for alarms, alarm in local._rds_instances : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name          = "URG:${local.name_prefix}RDSInstance-CPU-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = each.value.urgent.cpu_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.cpu_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.urgent.cpu_threshold_percent
  alarm_description   = "${each.key} RDS instance CPU alarm"
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_instance_cpu_casual" {
  for_each = { for alarms, alarm in local._rds_instances : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name          = "CAS:${local.name_prefix}RDSInstance-CPU-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = each.value.casual.cpu_evaluation_period_seconds
  evaluation_periods  = each.value.casual.cpu_evaluation_periods_count
  statistic           = "Maximum"
  threshold           = each.value.casual.cpu_threshold_percent
  alarm_description   = "${each.key} RDS instance CPU alarm"
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_instance_disk_urgent" {
  for_each = { for alarms, alarm in local._rds_instances : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name          = "URG:${local.name_prefix}RDSInstance-Disk-${each.key}"
  comparison_operator = "LessThanOrEqualToThreshold"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = each.value.urgent.disk_space_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.disk_space_evaluation_periods_count
  statistic           = "Average"
  threshold           = each.value.urgent.min_free_storage_percent * data.aws_db_instance.this[each.key].allocated_storage * pow(1024, 3) / 100
  alarm_description   = "${each.key} RDS instance free disk space alarm"
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_instance_disk_casual" {
  for_each = { for alarms, alarm in local._rds_instances : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name          = "CAS:${local.name_prefix}RDSInstance-Disk-${each.key}"
  comparison_operator = "LessThanOrEqualToThreshold"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = each.value.casual.disk_space_evaluation_period_seconds
  evaluation_periods  = each.value.casual.disk_space_evaluation_periods_count
  statistic           = "Average"
  threshold           = each.value.casual.min_free_storage_percent * data.aws_db_instance.this[each.key].allocated_storage * pow(1024, 3) / 100
  alarm_description   = "${each.key} RDS instance free disk space alarm"
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "ignore"

  dimensions = {
    DBInstanceIdentifier = each.key
  }
}

data "aws_db_instance" "this" {
  for_each               = local._rds_instances
  db_instance_identifier = each.key
}
