locals {
  _elasticsearch = {
    for e in var.elasticsearch :
    e.name => {
      urgent    = merge(var.elasticsearch_default_thresholds.urgent, lookup(e, "urgent", {}))
      casual    = merge(var.elasticsearch_default_thresholds.casual, lookup(e, "casual", {}))
      client_id = lookup(e, "client_id", "")
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_freestoragespace_urgent" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Elasticsearch-FreeStorageSpace-${each.key}"
  alarm_description = "Elasticsearch free storage space too low"

  namespace   = "AWS/ES"
  metric_name = "FreeStorageSpace"

  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "3"
  period              = "60"
  statistic           = "Minimum"
  threshold           = each.value.urgent.min_free_storage_space

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_freestoragespace_casual" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Elasticsearch-FreeStorageSpace-${each.key}"
  alarm_description = "Elasticsearch free storage space too low"

  namespace   = "AWS/ES"
  metric_name = "FreeStorageSpace"

  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "3"
  period              = "60"
  statistic           = "Minimum"
  threshold           = each.value.casual.min_free_storage_space

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_clusterindexwritesblocked_urgent" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Elasticsearch-ClusterIndexWritesBlocked-${each.key}"
  alarm_description = "Elasticsearch writes blocked"

  namespace   = "AWS/ES"
  metric_name = "ClusterIndexWritesBlocked"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  period              = "300"
  statistic           = "Average"
  threshold           = 0

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_clusterindexwritesblocked_casual" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Elasticsearch-ClusterIndexWritesBlocked-${each.key}"
  alarm_description = "Elasticsearch writes blocked"

  namespace   = "AWS/ES"
  metric_name = "ClusterIndexWritesBlocked"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  period              = "300"
  statistic           = "Average"
  threshold           = 0

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_clusterstatusred_urgent" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Elasticsearch-ClusterStatusRed-${each.key}"
  alarm_description = "Elasticsearch cluster status is red"

  namespace   = "AWS/ES"
  metric_name = "ClusterStatus.red"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.urgent.cluster_red_evaluation_periods
  datapoints_to_alarm = each.value.urgent.cluster_red_datapoints_to_alarm
  period              = "300"
  statistic           = "Maximum"
  threshold           = 0

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_clusterstatusred_casual" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Elasticsearch-ClusterStatusRed-${each.key}"
  alarm_description = "Elasticsearch cluster status is red"

  namespace   = "AWS/ES"
  metric_name = "ClusterStatus.red"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.casual.cluster_red_evaluation_periods
  datapoints_to_alarm = each.value.casual.cluster_red_datapoints_to_alarm
  period              = "300"
  statistic           = "Maximum"
  threshold           = 0

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_clusterstatusyellow_urgent" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Elasticsearch-ClusterStatusYellow-${each.key}"
  alarm_description = "Elasticsearch cluster status is yellow"

  namespace   = "AWS/ES"
  metric_name = "ClusterStatus.yellow"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.urgent.cluster_yellow_evaluation_periods
  datapoints_to_alarm = each.value.urgent.cluster_yellow_datapoints_to_alarm
  period              = "300"
  statistic           = "Maximum"
  threshold           = 0

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_clusterstatusyellow_casual" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Elasticsearch-ClusterStatusYellow-${each.key}"
  alarm_description = "Elasticsearch cluster status is yellow"

  namespace   = "AWS/ES"
  metric_name = "ClusterStatus.yellow"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.casual.cluster_yellow_evaluation_periods
  datapoints_to_alarm = each.value.casual.cluster_yellow_datapoints_to_alarm
  period              = "300"
  statistic           = "Maximum"
  threshold           = 0

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_cpuutilization_urgent" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Elasticsearch-CPUUtilization-${each.key}"
  alarm_description = "Elasticsearch CPU utilization too high"

  namespace   = "AWS/ES"
  metric_name = "CPUUtilization"

  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  period              = "120"
  statistic           = "Maximum"
  threshold           = each.value.urgent.max_cpu_utilization

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_cpuutilization_casual" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Elasticsearch-CPUUtilization-${each.key}"
  alarm_description = "Elasticsearch CPU utilization too high"

  namespace   = "AWS/ES"
  metric_name = "CPUUtilization"

  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  period              = "120"
  statistic           = "Maximum"
  threshold           = each.value.casual.max_cpu_utilization

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_jvmmemorypressure_urgent" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Elasticsearch-JVMMemoryPressure-${each.key}"
  alarm_description = "Elasticsearch JVM memory pressure too high"

  namespace   = "AWS/ES"
  metric_name = "JVMMemoryPressure"

  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  period              = "300"
  statistic           = "Average"
  threshold           = each.value.urgent.max_jvm_memory_pressure

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "elasticsearch_jvmmemorypressure_casual" {
  for_each = { for alarms, alarm in local._elasticsearch : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Elasticsearch-JVMMemoryPressure-${each.key}"
  alarm_description = "Elasticsearch JVM memory pressure too high"

  namespace   = "AWS/ES"
  metric_name = "JVMMemoryPressure"

  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "3"
  period              = "300"
  statistic           = "Average"
  threshold           = each.value.casual.max_jvm_memory_pressure

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ClientId   = each.value.client_id
    DomainName = each.key
  }
}
