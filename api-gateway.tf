locals {
  _api_gateways = {
    for a in var.api_gateways :
    a.name => {
      urgent = merge(var.api_gateway_default_thresholds.urgent, lookup(a, "urgent", {}))
      casual = merge(var.api_gateway_default_thresholds.casual, lookup(a, "casual", {}))
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "api_gateway_4xx_error_rate_urgent" {
  for_each = { for alarms, alarm in local._api_gateways : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}APIGateway-4xx-Error-Rate-${each.key}"
  alarm_description = "The 4xx error rate for the ${each.key} API gateway."

  namespace   = "AWS/ApiGateway"
  metric_name = "4XXError"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.urgent.evaluation_period_count
  period              = each.value.urgent.evaluation_period_seconds
  statistic           = "Average"
  threshold           = each.value.urgent.max_4xx_error_rate_percent
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ApiName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "api_gateway_4xx_error_rate_casual" {
  for_each = { for alarms, alarm in local._api_gateways : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}APIGateway-4xx-Error-Rate-${each.key}"
  alarm_description = "The 4xx error rate for the ${each.key} API gateway."

  namespace   = "AWS/ApiGateway"
  metric_name = "4XXError"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.casual.evaluation_period_count
  period              = each.value.casual.evaluation_period_seconds
  statistic           = "Average"
  threshold           = each.value.casual.max_4xx_error_rate_percent
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ApiName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "api_gateway_5xx_error_rate_urgent" {
  for_each = { for alarms, alarm in local._api_gateways : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}APIGateway-5xx-Error-Rate-${each.key}"
  alarm_description = "The 5xx error rate for the ${each.key} API gateway."

  namespace   = "AWS/ApiGateway"
  metric_name = "5XXError"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.urgent.evaluation_period_count
  period              = each.value.urgent.evaluation_period_seconds
  statistic           = "Average"
  threshold           = each.value.urgent.max_5xx_error_rate_percent
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ApiName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "api_gateway_5xx_error_rate_casual" {
  for_each = { for alarms, alarm in local._api_gateways : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}APIGateway-5xx-Error-Rate-${each.key}"
  alarm_description = "The 5xx error rate for the ${each.key} API gateway."

  namespace   = "AWS/ApiGateway"
  metric_name = "5XXError"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.casual.evaluation_period_count
  period              = each.value.casual.evaluation_period_seconds
  statistic           = "Average"
  threshold           = each.value.casual.max_5xx_error_rate_percent
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ApiName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "api_gateway_max_integration_latency_urgent" {
  for_each = { for alarms, alarm in local._api_gateways : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}APIGateway-integration-latency-${each.key}"
  alarm_description = "The time for the backend to respond to requests from API gateway for the ${each.key} API gateway."

  namespace   = "AWS/ApiGateway"
  metric_name = "IntegrationLatency"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.urgent.evaluation_period_count
  period              = each.value.urgent.evaluation_period_seconds
  statistic           = "Average"
  threshold           = each.value.urgent.max_integration_latency_millis
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ApiName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "api_gateway_max_integration_latency_casual" {
  for_each = { for alarms, alarm in local._api_gateways : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}APIGateway-integration-latency-${each.key}"
  alarm_description = "The time for the backend to respond to requests from API gateway for the ${each.key} API gateway."

  namespace   = "AWS/ApiGateway"
  metric_name = "IntegrationLatency"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.casual.evaluation_period_count
  period              = each.value.casual.evaluation_period_seconds
  statistic           = "Average"
  threshold           = each.value.casual.max_integration_latency_millis
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ApiName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "api_gateway_max_latency_urgent" {
  for_each = { for alarms, alarm in local._api_gateways : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}APIGateway-latency-${each.key}"
  alarm_description = "The response latency from API gateway receiving a request and responding for the ${each.key} API gateway."

  namespace   = "AWS/ApiGateway"
  metric_name = "Latency"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.urgent.evaluation_period_count
  period              = each.value.urgent.evaluation_period_seconds
  statistic           = "Average"
  threshold           = each.value.urgent.max_latency_millis
  alarm_actions       = [var.urgent_sns_topic_arn]
  ok_actions          = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ApiName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "api_gateway_max_latency_casual" {
  for_each = { for alarms, alarm in local._api_gateways : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}APIGateway-latency-${each.key}"
  alarm_description = "The response latency from API gateway receiving a request and responding for the ${each.key} API gateway."

  namespace   = "AWS/ApiGateway"
  metric_name = "Latency"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = each.value.casual.evaluation_period_count
  period              = each.value.casual.evaluation_period_seconds
  statistic           = "Average"
  threshold           = each.value.casual.max_latency_millis
  alarm_actions       = [var.casual_sns_topic_arn]
  ok_actions          = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    ApiName = each.key
  }
}
