# Namespace: AWS/EFS
locals {
  _efs = {
    for e in var.efs_volumes : e.file_system_id => {
      urgent = merge(var.efs_default_thresholds.urgent, lookup(e, "urgent", {}))
      casual = merge(var.efs_default_thresholds.casual, lookup(e, "casual", {}))
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "efs_burstcreditbalance_urgent" {
  for_each = { for alarms, alarm in local._efs : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}EFS-BurstCreditBalance-${each.key}"
  alarm_description = "EFS burst credit balance too low"

  namespace   = "AWS/EFS"
  metric_name = "BurstCreditBalance"

  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "3"
  period              = "300"
  statistic           = "Average"
  threshold           = each.value.urgent.min_burst_credit_balance

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    FileSystemId = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "efs_burstcreditbalance_casual" {
  for_each = { for alarms, alarm in local._efs : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}EFS-BurstCreditBalance-${each.key}"
  alarm_description = "EFS burst credit balance too low"

  namespace   = "AWS/EFS"
  metric_name = "BurstCreditBalance"

  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "3"
  period              = "300"
  statistic           = "Average"
  threshold           = each.value.casual.min_burst_credit_balance

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    FileSystemId = each.key
  }
}
