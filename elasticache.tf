# Namespace: AWS/ElastiCache
locals {
  _redis = {
    for r in var.redis : r.name => {
      urgent = merge(var.redis_default_thresholds.urgent, lookup(r, "urgent", {}))
      casual = merge(var.redis_default_thresholds.casual, lookup(r, "casual", {}))
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "redis_enginecpu_urgent" {
  for_each = { for alarms, alarm in local._redis : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Redis-EngineCPU-${each.key}"
  alarm_description = "Redis engine CPU too high"

  namespace   = "AWS/ElastiCache"
  metric_name = "EngineCPUUtilization"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.urgent.max_engine_cpu_percent

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    CacheClusterId = split(":", each.key)[0]
    CacheNodeId    = split(":", each.key)[1]
  }
}

resource "aws_cloudwatch_metric_alarm" "redis_enginecpu_casual" {
  for_each = { for alarms, alarm in local._redis : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Redis-EngineCPU-${each.key}"
  alarm_description = "Redis engine CPU too high"

  namespace   = "AWS/ElastiCache"
  metric_name = "EngineCPUUtilization"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.casual.max_engine_cpu_percent

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    CacheClusterId = split(":", each.key)[0]
    CacheNodeId    = split(":", each.key)[1]
  }
}

resource "aws_cloudwatch_metric_alarm" "redis_cpu_urgent" {
  for_each = { for alarms, alarm in local._redis : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Redis-CPU-${each.key}"
  alarm_description = "Redis CPU too high"

  namespace   = "AWS/ElastiCache"
  metric_name = "CPUUtilization"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.urgent.max_cpu_percent

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    CacheClusterId = split(":", each.key)[0]
    CacheNodeId    = split(":", each.key)[1]
  }
}

resource "aws_cloudwatch_metric_alarm" "redis_cpu_casual" {
  for_each = { for alarms, alarm in local._redis : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Redis-CPU-${each.key}"
  alarm_description = "Redis CPU too high"

  namespace   = "AWS/ElastiCache"
  metric_name = "CPUUtilization"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.casual.max_cpu_percent

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    CacheClusterId = split(":", each.key)[0]
    CacheNodeId    = split(":", each.key)[1]
  }
}

resource "aws_cloudwatch_metric_alarm" "redis_memory_urgent" {
  for_each = { for alarms, alarm in local._redis : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Redis-Memory-${each.key}"
  alarm_description = "Redis free memory too low"

  namespace   = "AWS/ElastiCache"
  metric_name = "FreeableMemory"

  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.urgent.min_freeable_mem_bytes

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    CacheClusterId = split(":", each.key)[0]
    CacheNodeId    = split(":", each.key)[1]
  }
}

resource "aws_cloudwatch_metric_alarm" "redis_memory_casual" {
  for_each = { for alarms, alarm in local._redis : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Redis-Memory-${each.key}"
  alarm_description = "Redis free memory too low"

  namespace   = "AWS/ElastiCache"
  metric_name = "FreeableMemory"

  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.casual.min_freeable_mem_bytes

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    CacheClusterId = split(":", each.key)[0]
    CacheNodeId    = split(":", each.key)[1]
  }
}
