# Monitors for DynamoDB tables, including:
# System Errors - "An HTTP 500 usually indicates an internal service error."
# AverageLatency - DynamnoDB "....delivers single-digit millisecond performance at any scale" - so longer response times may indicate an underlying problem
# Thoughput - There are limits on throughput (https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Limits.html). Breaching limits will result in throttling and will degrade the user experience.

locals {
  _dynamo_db_tables = {
    for dynamo_db_table in var.dynamo_db_tables :
    dynamo_db_table.name => {
      urgent = merge(var.dynamo_db_table_defaults.urgent, lookup(dynamo_db_table, "urgent", {}))
      casual = merge(var.dynamo_db_table_defaults.casual, lookup(dynamo_db_table, "casual", {}))
      common = merge(var.dynamo_db_table_defaults.common, lookup(dynamo_db_table, "common", {}))
    }
  }
}

# System Errors
resource "aws_cloudwatch_metric_alarm" "dynamo_db_system_errors_urgent" {
  for_each = { for alarms, alarm in local._dynamo_db_tables : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}${each.key}-${each.value.common.system_error_alarm_name}"
  alarm_description = "${each.key} - ${each.value.common.system_error_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "SystemErrors"
  dimensions = {
    TableName = each.key
  }
  period              = each.value.urgent.system_error_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.system_error_evaluation_period_count
  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = each.value.urgent.system_error_threshold

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = each.value.urgent.system_error_missing_data
}

resource "aws_cloudwatch_metric_alarm" "dynamo_db_system_errors_casual" {
  for_each = { for alarms, alarm in local._dynamo_db_tables : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}${each.key}-${each.value.common.system_error_alarm_name}"
  alarm_description = "${each.key} - ${each.value.common.system_error_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "SystemErrors"
  dimensions = {
    TableName = each.key
  }
  period              = each.value.casual.system_error_evaluation_period_seconds
  evaluation_periods  = each.value.casual.system_error_evaluation_period_count
  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = each.value.casual.system_error_threshold

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = each.value.casual.system_error_missing_data
}

# Latency
resource "aws_cloudwatch_metric_alarm" "dynamo_db_latency_urgent" {
  for_each = { for alarms, alarm in local._dynamo_db_tables : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}${each.key}-${each.value.common.latency_alarm_name}"
  alarm_description = "${each.key} - ${each.value.common.latency_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "SuccessfulRequestLatency"
  dimensions = {
    TableName = each.key
  }
  period              = each.value.urgent.latency_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.latency_evaluation_period_count
  statistic           = "Average"
  comparison_operator = "GreaterThanThreshold"
  threshold           = each.value.urgent.latency_threshold_ms

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = each.value.urgent.latency_missing_data
}

resource "aws_cloudwatch_metric_alarm" "dynamo_db_latency_casual" {
  for_each = { for alarms, alarm in local._dynamo_db_tables : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}${each.key}-${each.value.common.latency_alarm_name}"
  alarm_description = "${each.key} - ${each.value.common.latency_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "SuccessfulRequestLatency"
  dimensions = {
    TableName = each.key
  }
  period              = each.value.casual.latency_evaluation_period_seconds
  evaluation_periods  = each.value.casual.latency_evaluation_period_count
  statistic           = "Average"
  comparison_operator = "GreaterThanThreshold"
  threshold           = each.value.casual.latency_threshold_ms

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = each.value.casual.latency_missing_data
}

# Throughput at account level
resource "aws_cloudwatch_metric_alarm" "dynamo_db_account_read_throughput_urgent" {
  count = var.enable_urgent && var.dynamo_db_account_defaults.urgent.enabled && length(var.dynamo_db_tables) > 0 ? 1 : 0

  alarm_name        = "URG:${local.name_prefix}-account-reads-${var.dynamo_db_account_defaults.common.throughput_alarm_name}"
  alarm_description = "account level reads- ${var.dynamo_db_account_defaults.common.throughput_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "AccountProvisionedReadCapacityUtilization"

  period              = var.dynamo_db_account_defaults.urgent.throughput_evaluation_period_seconds
  evaluation_periods  = var.dynamo_db_account_defaults.urgent.throughput_evaluation_period_count
  statistic           = "Maximum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = var.dynamo_db_account_defaults.urgent.throughput_threshold_percent

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = var.dynamo_db_account_defaults.urgent.throughput_missing_data
}

resource "aws_cloudwatch_metric_alarm" "dynamo_db_account_read_throughput_casual" {
  count = var.enable_urgent && var.dynamo_db_account_defaults.casual.enabled && length(var.dynamo_db_tables) > 0 ? 1 : 0

  alarm_name        = "CAS:${local.name_prefix}-account-reads-${var.dynamo_db_account_defaults.common.throughput_alarm_name}"
  alarm_description = "account level reads- ${var.dynamo_db_account_defaults.common.throughput_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "AccountProvisionedReadCapacityUtilization"

  period              = var.dynamo_db_account_defaults.casual.throughput_evaluation_period_seconds
  evaluation_periods  = var.dynamo_db_account_defaults.casual.throughput_evaluation_period_count
  statistic           = "Maximum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = var.dynamo_db_account_defaults.casual.throughput_threshold_percent

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = var.dynamo_db_account_defaults.casual.throughput_missing_data
}

resource "aws_cloudwatch_metric_alarm" "dynamo_db_account_write_throughput_urgent" {
  count = var.enable_urgent && var.dynamo_db_account_defaults.urgent.enabled && length(var.dynamo_db_tables) > 0 ? 1 : 0

  alarm_name        = "URG:${local.name_prefix}-account-writes-${var.dynamo_db_account_defaults.common.throughput_alarm_name}"
  alarm_description = "account level writes- ${var.dynamo_db_account_defaults.common.throughput_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "AccountProvisionedWriteCapacityUtilization"

  period              = var.dynamo_db_account_defaults.urgent.throughput_evaluation_period_seconds
  evaluation_periods  = var.dynamo_db_account_defaults.urgent.throughput_evaluation_period_count
  statistic           = "Maximum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = var.dynamo_db_account_defaults.urgent.throughput_threshold_percent

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = var.dynamo_db_account_defaults.urgent.throughput_missing_data
}

resource "aws_cloudwatch_metric_alarm" "dynamo_db_account_write_throughput_casual" {
  count = var.enable_urgent && var.dynamo_db_account_defaults.casual.enabled && length(var.dynamo_db_tables) > 0 ? 1 : 0

  alarm_name        = "CAS:${local.name_prefix}-account-writes-${var.dynamo_db_account_defaults.common.throughput_alarm_name}"
  alarm_description = "account level writes - ${var.dynamo_db_account_defaults.common.throughput_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "AccountProvisionedWriteCapacityUtilization"

  period              = var.dynamo_db_account_defaults.casual.throughput_evaluation_period_seconds
  evaluation_periods  = var.dynamo_db_account_defaults.casual.throughput_evaluation_period_count
  statistic           = "Maximum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = var.dynamo_db_account_defaults.casual.throughput_threshold_percent

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = var.dynamo_db_account_defaults.casual.throughput_missing_data
}

# Throughput at table level
resource "aws_cloudwatch_metric_alarm" "dynamo_db_table_read_throughput_urgent" {
  for_each = { for alarms, alarm in local._dynamo_db_tables : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}${each.key}-reads-${var.dynamo_db_account_defaults.common.throughput_alarm_name}"
  alarm_description = "${each.key} - reads - ${var.dynamo_db_account_defaults.common.throughput_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "ConsumedReadCapacityUnits"

  dimensions = {
    TableName = each.key
  }

  period              = each.value.urgent.throughput_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.throughput_evaluation_period_count
  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = (each.value.urgent.throughput_threshold * each.value.urgent.throughput_evaluation_period_seconds)

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = each.value.urgent.throughput_missing_data
}

resource "aws_cloudwatch_metric_alarm" "dynamo_db_table_read_throughput_casual" {
  for_each = { for alarms, alarm in local._dynamo_db_tables : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}${each.key}-reads-${var.dynamo_db_account_defaults.common.throughput_alarm_name}"
  alarm_description = "${each.key} - reads - ${var.dynamo_db_account_defaults.common.throughput_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "ConsumedReadCapacityUnits"

  dimensions = {
    TableName = each.key
  }

  period              = each.value.casual.throughput_evaluation_period_seconds
  evaluation_periods  = each.value.casual.throughput_evaluation_period_count
  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = (each.value.casual.throughput_threshold * each.value.casual.throughput_evaluation_period_seconds)

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = each.value.casual.throughput_missing_data
}

resource "aws_cloudwatch_metric_alarm" "dynamo_db_table_write_throughput_urgent" {
  for_each = { for alarms, alarm in local._dynamo_db_tables : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}${each.key}-writes-${var.dynamo_db_account_defaults.common.throughput_alarm_name}"
  alarm_description = "${each.key} - writes - ${var.dynamo_db_account_defaults.common.throughput_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "ConsumedWriteCapacityUnits"

  dimensions = {
    TableName = each.key
  }

  period              = each.value.urgent.throughput_evaluation_period_seconds
  evaluation_periods  = each.value.urgent.throughput_evaluation_period_count
  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = (each.value.urgent.throughput_threshold * each.value.urgent.throughput_evaluation_period_seconds)

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = each.value.urgent.throughput_missing_data
}

resource "aws_cloudwatch_metric_alarm" "dynamo_db_table_write_throughput_casual" {
  for_each = { for alarms, alarm in local._dynamo_db_tables : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}${each.key}-writes-${var.dynamo_db_account_defaults.common.throughput_alarm_name}"
  alarm_description = "${each.key} - writes - ${var.dynamo_db_account_defaults.common.throughput_alarm_description}"

  namespace   = "AWS/DynamoDB"
  metric_name = "ConsumedWriteCapacityUnits"

  dimensions = {
    TableName = each.key
  }

  period              = each.value.casual.throughput_evaluation_period_seconds
  evaluation_periods  = each.value.casual.throughput_evaluation_period_count
  statistic           = "Sum"
  comparison_operator = "GreaterThanThreshold"
  threshold           = (each.value.casual.throughput_threshold * each.value.casual.throughput_evaluation_period_seconds)

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = each.value.casual.throughput_missing_data
}
