locals {
  _lb = {
    for l in var.lb :
    l.name => {
      urgent = merge(var.lb_defaults.urgent, lookup(l, "urgent", {}))
      casual = merge(var.lb_defaults.casual, lookup(l, "casual", {}))
      type   = lookup(l, "type", var.lb_defaults.type)
    }
  }
  metric_names = {
    "elb" = {
      errors = "HTTPCode_ELB_5XX"
    }
    "alb" = {
      errors = "HTTPCode_ELB_5XX_Count"
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "lb_error_rate_urgent" {
  for_each = { for alarms, alarm in local._lb : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}${each.key}-LB-ErrorRate"
  alarm_description = "LB ${each.key}: error rate too high"

  metric_query {
    id = "errors"

    metric {
      namespace   = "AWS/ELB"
      metric_name = local.metric_names[each.value.type].errors
      dimensions = {
        "LoadBalancerName" = each.key
      }

      stat   = "Sum"
      unit   = "Count"
      period = "60"
    }
  }

  metric_query {
    id = "requests"

    metric {
      namespace   = "AWS/ELB"
      metric_name = "RequestCount"
      dimensions = {
        "LoadBalancerName" = each.key
      }

      stat   = "Sum"
      unit   = "Count"
      period = "60"
    }
  }

  metric_query {
    id          = "error_rate"
    expression  = "errors/requests*100"
    label       = "Error Rate"
    return_data = "true"
  }

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  threshold           = each.value.urgent.max_error_rate_percent

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "lb_error_rate_casual" {
  for_each = { for alarms, alarm in local._lb : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}${each.key}-LB-ErrorRate"
  alarm_description = "LB ${each.key}: error rate too high"

  metric_query {
    id = "errors"

    metric {
      namespace   = "AWS/ELB"
      metric_name = local.metric_names[each.value.type].errors
      dimensions = {
        "LoadBalancerName" = each.key
      }

      stat   = "Sum"
      unit   = "Count"
      period = "60"
    }
  }

  metric_query {
    id = "requests"

    metric {
      namespace   = "AWS/ELB"
      metric_name = "RequestCount"
      dimensions = {
        "LoadBalancerName" = each.key
      }

      stat   = "Sum"
      unit   = "Count"
      period = "60"
    }
  }

  metric_query {
    id          = "error_rate"
    expression  = "errors/requests*100"
    label       = "Error Rate"
    return_data = "true"
  }

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  threshold           = each.value.casual.max_error_rate_percent

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"
}
