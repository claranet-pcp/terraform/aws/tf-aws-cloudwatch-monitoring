locals {
  _lambda = {
    for l in var.lambda :
    l.name => {
      urgent = merge(var.lambda_default_thresholds.urgent, lookup(l, "urgent", {}))
      casual = merge(var.lambda_default_thresholds.casual, lookup(l, "casual", {}))
    }
  }
}
resource "aws_cloudwatch_metric_alarm" "lambda_error_rate_urgent" {
  for_each = { for alarms, alarm in local._lambda : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Lambda-ErrorRate-${each.key}"
  alarm_description = "Lambda function error rate too high"

  metric_query {
    id = "errors"

    metric {
      namespace   = "AWS/Lambda"
      metric_name = "Errors"
      dimensions = {
        "FunctionName" = each.key
      }

      stat   = "Sum"
      unit   = "Count"
      period = "60"
    }
  }

  metric_query {
    id = "requests"

    metric {
      namespace   = "AWS/Lambda"
      metric_name = "Invocations"
      dimensions = {
        "FunctionName" = each.key
      }

      stat   = "Sum"
      unit   = "Count"
      period = "60"
    }
  }

  metric_query {
    id          = "error_rate"
    expression  = "errors/requests*100"
    label       = "Error Rate"
    return_data = "true"
  }

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  threshold           = each.value.urgent.max_error_rate_percent

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "lambda_error_rate_casual" {
  for_each = { for alarms, alarm in local._lambda : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Lambda-ErrorRate-${each.key}"
  alarm_description = "Lambda function error rate too high"

  metric_query {
    id = "errors"

    metric {
      namespace   = "AWS/Lambda"
      metric_name = "Errors"
      dimensions = {
        "FunctionName" = each.key
      }

      stat   = "Sum"
      unit   = "Count"
      period = "60"
    }
  }

  metric_query {
    id = "requests"

    metric {
      namespace   = "AWS/Lambda"
      metric_name = "Invocations"
      dimensions = {
        "FunctionName" = each.key
      }

      stat   = "Sum"
      unit   = "Count"
      period = "60"
    }
  }

  metric_query {
    id          = "error_rate"
    expression  = "errors/requests*100"
    label       = "Error Rate"
    return_data = "true"
  }

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  threshold           = each.value.casual.max_error_rate_percent

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"
}


resource "aws_cloudwatch_metric_alarm" "lambda_throttle_rate_urgent" {
  for_each = { for alarms, alarm in local._lambda : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}Lambda-ThrottleRate-${each.key}"
  alarm_description = "Lambda function throttle rate too high"

  namespace   = "AWS/Lambda"
  metric_name = "Throttles"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Sum"
  threshold           = each.value.urgent.max_5m_throttle_rate

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    "FunctionName" = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "lambda_throttle_rate_casual" {
  for_each = { for alarms, alarm in local._lambda : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}Lambda-ThrottleRate-${each.key}"
  alarm_description = "Lambda function throttle rate too high"

  namespace   = "AWS/Lambda"
  metric_name = "Throttles"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Sum"
  threshold           = each.value.casual.max_5m_throttle_rate

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    "FunctionName" = each.key
  }
}
