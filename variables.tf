variable "enable_urgent" {
  description = "Set true to enable urgent alarms."
  type        = bool
  default     = false
}

variable "enable_casual" {
  description = "Set true to enable casual alarms."
  type        = bool
  default     = true
}

variable "name_prefix" {
  description = "Arbitrary string to keep names unique."
  type        = string
  default     = ""
}

locals {
  name_prefix = "%{if var.name_prefix != ""}${var.name_prefix}-%{endif}"
}


## Target SQS endpoint for casual alarms
variable "casual_sns_topic_arn" {
  description = "SNS topic ARN for sending casual (nonurgent) alarms to."
  type        = string
}

## Target SQS endpoint for urgent alarms
variable "urgent_sns_topic_arn" {
  description = "SNS topic ARN for sending urgent alarms to."
  type        = string
}

## Api Gateway monitoring
# api_gateways = [
#   name = "my-api-name"
#   urgent = {
#     max_4xx_error_rate_percent = 90
#   }
#   casual = {
#     max_integration_latency_millis = 1000
#   }
# ]
variable "api_gateways" {
  description = "A map of api gateways which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "api_gateway_default_thresholds" {
  description = "Default thresholds for API Gateway functions."
  default = {
    urgent = {
      enabled                        = true
      max_4xx_error_rate_percent     = 40
      max_5xx_error_rate_percent     = 10
      max_integration_latency_millis = 3000
      max_latency_millis             = 3000
      evaluation_period_seconds      = 60
      evaluation_period_count        = 5
    }
    casual = {
      enabled                        = true
      max_4xx_error_rate_percent     = 20
      max_5xx_error_rate_percent     = 5
      max_integration_latency_millis = 1500
      max_latency_millis             = 1500
      evaluation_period_seconds      = 60
      evaluation_period_count        = 5
    }
  }
}

## Elasticache, Redis monitoring
# var.redis = [
#  {
#     name = "cluster1:node0001"
#     urgent = {
#       max_cpu_percent = 80
#     }
#   },
#  {
#     name = "cluster1:node0002"
#     urgent = {
#       max_cpu_percent = 80
#     }
#   },
#   {
#     name = "cluster2:node001"
#   },
# ]
variable "redis" {
  description = "List of maps containing redis nodes which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "redis_default_thresholds" {
  description = "Default thresholds for Redis node."
  type = object({
    urgent = object({
      enabled                = bool
      max_engine_cpu_percent = number
      max_cpu_percent        = number
      min_freeable_mem_bytes = number
    })
    casual = object({
      enabled                = bool
      max_engine_cpu_percent = number
      max_cpu_percent        = number
      min_freeable_mem_bytes = number
    })
  })
  default = {
    urgent = {
      enabled                = true
      max_engine_cpu_percent = 90
      max_cpu_percent        = 90
      min_freeable_mem_bytes = 268435456
    }
    casual = {
      enabled                = true
      max_engine_cpu_percent = 75
      max_cpu_percent        = 75
      min_freeable_mem_bytes = 536870912
    }
  }
}

## Elasticsearch monitoring
variable "elasticsearch" {
  description = "List of maps containing elasticsearch clusters which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "elasticsearch_default_thresholds" {
  description = "Default thresholds for Elasticsearch clusters."
  type = object({
    urgent = object({
      enabled                            = bool
      min_free_storage_space             = number
      max_cpu_utilization                = number
      max_jvm_memory_pressure            = number
      cluster_yellow_evaluation_periods  = number
      cluster_yellow_datapoints_to_alarm = number
      cluster_red_evaluation_periods     = number
      cluster_red_datapoints_to_alarm    = number
    })
    casual = object({
      enabled                            = bool
      min_free_storage_space             = number
      max_cpu_utilization                = number
      max_jvm_memory_pressure            = number
      cluster_yellow_evaluation_periods  = number
      cluster_yellow_datapoints_to_alarm = number
      cluster_red_evaluation_periods     = number
      cluster_red_datapoints_to_alarm    = number
    })
    # AWS account ID where the Elasticsearch cluster resides
    client_id = string
  })
  default = {
    urgent = {
      enabled                            = true
      min_free_storage_space             = 10000
      max_cpu_utilization                = 90
      max_jvm_memory_pressure            = 90
      cluster_yellow_evaluation_periods  = 1
      cluster_yellow_datapoints_to_alarm = 1
      cluster_red_evaluation_periods     = 1
      cluster_red_datapoints_to_alarm    = 1
    }
    casual = {
      enabled                            = true
      min_free_storage_space             = 20000
      max_cpu_utilization                = 80
      max_jvm_memory_pressure            = 80
      cluster_yellow_evaluation_periods  = 1
      cluster_yellow_datapoints_to_alarm = 1
      cluster_red_evaluation_periods     = 1
      cluster_red_datapoints_to_alarm    = 1
    }
    client_id = ""
  }
}

## Lambda monitoring
variable "lambda" {
  description = "List of maps containing api gateways which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "lambda_default_thresholds" {
  description = "Default thresholds for Lambda functions."
  type = object({
    urgent = object({
      enabled                = bool
      max_error_rate_percent = number
      max_duration_percent   = number
      max_5m_throttle_rate   = number
    })
    casual = object({
      enabled                = bool
      max_error_rate_percent = number
      max_duration_percent   = number
      max_5m_throttle_rate   = number
    })
  })
  default = {
    urgent = {
      enabled                = true
      max_error_rate_percent = 10
      max_duration_percent   = 95
      max_5m_throttle_rate   = 10
    }
    casual = {
      enabled                = true
      max_error_rate_percent = 5
      max_duration_percent   = 80
      max_5m_throttle_rate   = 5
    }
  }
}

## LB monitoring
variable "lb" {
  description = "List of maps containing load balancer names which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "lb_defaults" {
  description = "Defaults for LBs."
  type = object({
    urgent = object({
      enabled                = bool
      max_error_rate_percent = number

    })
    casual = object({
      enabled                = bool
      max_error_rate_percent = number
    })
    type = string
  })
  default = {
    urgent = {
      enabled                = true
      max_error_rate_percent = 10
    }
    casual = {
      enabled                = true
      max_error_rate_percent = 5
    }
    type = "alb"
  }
}

## RDS cluster monitoring
variable "rds_clusters" {
  description = "A map containing RDS clusters which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "rds_clusters_default_thresholds" {
  description = "Default thresholds for RDS databases."
  type = object({
    urgent = object({
      enabled                                      = bool
      reader_connections_count_threshold           = number
      reader_connections_evaluation_period_seconds = number
      reader_connections_evaluation_periods_count  = number
      reader_cpu_evaluation_period_seconds         = number
      reader_cpu_evaluation_periods_count          = number
      reader_cpu_threshold_percent                 = number
      replica_lag_evaluation_period_seconds        = number
      replica_lag_evaluation_periods_count         = number
      replica_lag_threshold_millis                 = number
      writer_connections_count_threshold           = number
      writer_connections_evaluation_period_seconds = number
      writer_connections_evaluation_periods_count  = number
      writer_cpu_evaluation_period_seconds         = number
      writer_cpu_evaluation_periods_count          = number
      writer_cpu_threshold_percent                 = number
    })
    casual = object({
      enabled                                      = bool
      reader_connections_count_threshold           = number
      reader_connections_evaluation_period_seconds = number
      reader_connections_evaluation_periods_count  = number
      reader_cpu_evaluation_period_seconds         = number
      reader_cpu_evaluation_periods_count          = number
      reader_cpu_threshold_percent                 = number
      replica_lag_evaluation_period_seconds        = number
      replica_lag_evaluation_periods_count         = number
      replica_lag_threshold_millis                 = number
      writer_connections_count_threshold           = number
      writer_connections_evaluation_period_seconds = number
      writer_connections_evaluation_periods_count  = number
      writer_cpu_evaluation_period_seconds         = number
      writer_cpu_evaluation_periods_count          = number
      writer_cpu_threshold_percent                 = number
    })
  })

  default = {
    urgent = {
      enabled                                      = true
      reader_connections_count_threshold           = 1000
      reader_connections_evaluation_period_seconds = 60
      reader_connections_evaluation_periods_count  = 1
      reader_cpu_evaluation_period_seconds         = 60
      reader_cpu_evaluation_periods_count          = 1
      reader_cpu_threshold_percent                 = 95
      replica_lag_evaluation_period_seconds        = 60
      replica_lag_evaluation_periods_count         = 1
      replica_lag_threshold_millis                 = 100
      writer_connections_count_threshold           = 500
      writer_connections_evaluation_period_seconds = 60
      writer_connections_evaluation_periods_count  = 1
      writer_cpu_evaluation_period_seconds         = 60
      writer_cpu_evaluation_periods_count          = 1
      writer_cpu_threshold_percent                 = 95
    }
    casual = {
      enabled                                      = true
      reader_connections_count_threshold           = 500
      reader_connections_evaluation_period_seconds = 60
      reader_connections_evaluation_periods_count  = 1
      reader_cpu_evaluation_period_seconds         = 60
      reader_cpu_evaluation_periods_count          = 1
      reader_cpu_threshold_percent                 = 85
      replica_lag_evaluation_period_seconds        = 60
      replica_lag_evaluation_periods_count         = 1
      replica_lag_threshold_millis                 = 100
      writer_connections_count_threshold           = 500
      writer_connections_evaluation_period_seconds = 60
      writer_connections_evaluation_periods_count  = 1
      writer_cpu_evaluation_period_seconds         = 60
      writer_cpu_evaluation_periods_count          = 1
      writer_cpu_threshold_percent                 = 85
    }
  }
}

## RDS instance monitoring
variable "rds_instances" {
  description = "A list of maps containing RDS instances which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "rds_instances_default_thresholds" {
  description = "Default thresholds for RDS instances."
  type = object({
    urgent = object({
      enabled                               = bool
      connections_evaluation_period_seconds = number
      connections_evaluation_periods_count  = number
      connections_count_threshold           = number
      cpu_evaluation_period_seconds         = number
      cpu_evaluation_periods_count          = number
      cpu_threshold_percent                 = number
      disk_space_evaluation_period_seconds  = number
      disk_space_evaluation_periods_count   = number
      min_free_storage_percent              = number
    })
    casual = object({
      enabled                               = bool
      connections_evaluation_period_seconds = number
      connections_evaluation_periods_count  = number
      connections_count_threshold           = number
      cpu_evaluation_period_seconds         = number
      cpu_evaluation_periods_count          = number
      cpu_threshold_percent                 = number
      disk_space_evaluation_period_seconds  = number
      disk_space_evaluation_periods_count   = number
      min_free_storage_percent              = number
    })
  })
  default = {
    urgent = {
      enabled                               = true
      connections_evaluation_period_seconds = 60
      connections_evaluation_periods_count  = 1
      connections_count_threshold           = 1000
      cpu_evaluation_period_seconds         = 60
      cpu_evaluation_periods_count          = 5
      cpu_threshold_percent                 = 95
      disk_space_evaluation_period_seconds  = 60
      disk_space_evaluation_periods_count   = 5
      min_free_storage_percent              = 5
    }
    casual = {
      enabled                               = true
      connections_evaluation_period_seconds = 60
      connections_evaluation_periods_count  = 1
      connections_count_threshold           = 500
      cpu_evaluation_period_seconds         = 60
      cpu_evaluation_periods_count          = 5
      cpu_threshold_percent                 = 85
      disk_space_evaluation_period_seconds  = 60
      disk_space_evaluation_periods_count   = 5
      min_free_storage_percent              = 15
    }
  }
}

## SQS monitoring
variable "sqs" {
  description = "List of maps containing SQS queue which Cloudwatch alarms should be setup for."
  type        = list
  default     = []
}

variable "sqs_defaults" {
  description = "Default thresholds for SQS queue."
  type = object({
    urgent = object({
      enabled                  = bool
      oldest_message_seconds   = number
      inflight_message_percent = number
      max_message_size_percent = number
    })
    casual = object({
      enabled                  = bool
      oldest_message_seconds   = number
      inflight_message_percent = number
      max_message_size_percent = number
    })
    type = string
  })
  default = {
    urgent = {
      enabled                  = true
      oldest_message_seconds   = 7200
      inflight_message_percent = 95
      max_message_size_percent = 100
    }
    casual = {
      enabled                  = true
      oldest_message_seconds   = 3600
      inflight_message_percent = 85
      max_message_size_percent = 95
    }
    type = "normal"
  }
}

# Dynamo DB
variable "dynamo_db_tables" {
  description = "List of DynamoDB tables which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "dynamo_db_account_defaults" {
  description = "Account level defaults for DynamoDB alarms"
  type = object({
    urgent = object({
      enabled                              = bool
      throughput_threshold_percent         = number
      throughput_evaluation_period_seconds = number
      throughput_evaluation_period_count   = number
      throughput_missing_data              = string
    })
    casual = object({
      enabled                              = bool
      throughput_threshold_percent         = number
      throughput_evaluation_period_seconds = number
      throughput_evaluation_period_count   = number
      throughput_missing_data              = string
    })
    common = object({
      throughput_alarm_name        = string
      throughput_alarm_description = string
    })
  })
  default = {
    urgent = {
      enabled                              = true
      throughput_threshold_percent         = 80
      throughput_evaluation_period_seconds = 300
      throughput_evaluation_period_count   = 5
      throughput_missing_data              = "notBreaching"
    }
    casual = {
      enabled                              = true
      throughput_threshold_percent         = 70
      throughput_evaluation_period_seconds = 300
      throughput_evaluation_period_count   = 5
      throughput_missing_data              = "notBreaching"
    }
    common = {
      throughput_alarm_name        = "Dynamo-Throughput"
      throughput_alarm_description = "DynamoDB throughput is high - limits may be reached."
    }
  }
}

variable "dynamo_db_table_defaults" {
  description = "Table level defaults for DynamoDB alarms"
  type = object({
    urgent = object({
      enabled                                = bool
      system_error_threshold                 = number
      system_error_evaluation_period_seconds = number
      system_error_evaluation_period_count   = number
      system_error_missing_data              = string

      latency_threshold_ms              = number
      latency_evaluation_period_seconds = number
      latency_evaluation_period_count   = number
      latency_missing_data              = string

      throughput_threshold                 = number
      throughput_evaluation_period_seconds = number
      throughput_evaluation_period_count   = number
      throughput_missing_data              = string
    })
    casual = object({
      enabled                                = bool
      system_error_threshold                 = number
      system_error_evaluation_period_seconds = number
      system_error_evaluation_period_count   = number
      system_error_missing_data              = string

      latency_threshold_ms              = number
      latency_evaluation_period_seconds = number
      latency_evaluation_period_count   = number
      latency_missing_data              = string

      throughput_threshold                 = number
      throughput_evaluation_period_seconds = number
      throughput_evaluation_period_count   = number
      throughput_missing_data              = string
    })
    common = object({
      system_error_alarm_name        = string
      system_error_alarm_description = string

      latency_alarm_name        = string
      latency_alarm_description = string
    })
  })
  default = {
    urgent = {
      enabled                                = true
      system_error_threshold                 = 10
      system_error_evaluation_period_seconds = 60
      system_error_evaluation_period_count   = 5
      system_error_missing_data              = "notBreaching"

      latency_threshold_ms              = 250
      latency_evaluation_period_seconds = 60
      latency_evaluation_period_count   = 5
      latency_missing_data              = "notBreaching"

      throughput_threshold                 = 10000
      throughput_evaluation_period_seconds = 60
      throughput_evaluation_period_count   = 5
      throughput_missing_data              = "notBreaching"
    }
    casual = {
      enabled                                = true
      system_error_threshold                 = 5
      system_error_evaluation_period_seconds = 60
      system_error_evaluation_period_count   = 5
      system_error_missing_data              = "notBreaching"

      latency_threshold_ms              = 150
      latency_evaluation_period_seconds = 60
      latency_evaluation_period_count   = 5
      latency_missing_data              = "notBreaching"

      throughput_threshold                 = 1000
      throughput_evaluation_period_seconds = 60
      throughput_evaluation_period_count   = 5
      throughput_missing_data              = "notBreaching"
    }

    common = {
      system_error_alarm_name        = "Dynamo-SystemErrors"
      system_error_alarm_description = "DynamoDB table system errors too high"

      latency_alarm_name        = "Dynamo-Latency"
      latency_alarm_description = "DynamoDB latency too high"
    }
  }
}

## EFS monitoring
# efs_volumes = [
#   {
#     file_system_id = aws_efs_file_system.efs.id
#     urgent = {
#       min_burst_credit_balance = 1000000000000
#     }
#     casual = {
#       min_burst_credit_balance = 1500000000000
#     }
#   }
# ]
variable "efs_volumes" {
  description = "List of maps containing EFS volumes which Cloudwatch alarms should be setup for."
  type        = list(any)
  default     = []
}

variable "efs_default_thresholds" {
  description = "Default thresholds for EFS volumes."
  type = object({
    urgent = object({
      enabled                  = bool
      min_burst_credit_balance = number
    })
    casual = object({
      enabled                  = bool
      min_burst_credit_balance = number
    })
  })
  default = {
    urgent = {
      enabled                  = true
      min_burst_credit_balance = 1000000000000
    }
    casual = {
      enabled                  = true
      min_burst_credit_balance = 1500000000000
    }
  }
}

variable "datapoints_to_alarm" {
  description = "The number of datapoints that must be breaching to trigger the alarm."
  type        = string
  default     = ""
}
