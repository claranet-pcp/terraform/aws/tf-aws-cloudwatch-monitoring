locals {
  _sqs = {
    for s in var.sqs :
    s.name => {
      urgent = merge(var.sqs_defaults.urgent, lookup(s, "urgent", {}))
      casual = merge(var.sqs_defaults.casual, lookup(s, "casual", {}))
      type   = lookup(s, "type", var.sqs_defaults.type)
    }
  }
  # Hardcoded because not currently adjustable
  sqs_max_inflight_messages = {
    fifo   = 20000,
    normal = 120000,
  }
  sqs_message_size_max_bytes = 262144
}

resource "aws_cloudwatch_metric_alarm" "sqs_oldest_message_urgent" {
  for_each = { for alarms, alarm in local._sqs : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}${each.key}-OldMsg"
  alarm_description = "SQS queue ${each.key}: oldest message too old"

  namespace   = "AWS/SQS"
  metric_name = "ApproximateAgeOfOldestMessage"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.urgent.oldest_message_seconds

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    QueueName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_oldest_message_casual" {
  for_each = { for alarms, alarm in local._sqs : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}${each.key}-OldMsg"
  alarm_description = "SQS queue ${each.key}: oldest message too old"

  namespace   = "AWS/SQS"
  metric_name = "ApproximateAgeOfOldestMessage"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.casual.oldest_message_seconds

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    QueueName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_inflight_messages_urgent" {
  for_each = { for alarms, alarm in local._sqs : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}${each.key}-SQS-InflightMsgs"
  alarm_description = "SQS queue ${each.key}: too many inflight messages"

  namespace   = "AWS/SQS"
  metric_name = "ApproximateNumberOfMessagesNotVisible"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.urgent.inflight_message_percent * local.sqs_max_inflight_messages[each.value.type] / 100

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    QueueName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_inflight_messages_casual" {
  for_each = { for alarms, alarm in local._sqs : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}${each.key}-SQS-InflightMsgs"
  alarm_description = "SQS queue ${each.key}: too many inflight messages"

  namespace   = "AWS/SQS"
  metric_name = "ApproximateNumberOfMessagesNotVisible"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "5"
  period              = "60"
  statistic           = "Average"
  threshold           = each.value.casual.inflight_message_percent * local.sqs_max_inflight_messages[each.value.type] / 100

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    QueueName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_message_too_large_urgent" {
  for_each = { for alarms, alarm in local._sqs : alarms => alarm if alarm.urgent.enabled && var.enable_urgent }

  alarm_name        = "URG:${local.name_prefix}${each.key}-SQS-BigMsg"
  alarm_description = "SQS queue ${each.key}: message approaching size limit"

  namespace   = "AWS/SQS"
  metric_name = "SentMessageSize"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  period              = "3600"
  statistic           = "Maximum"
  threshold           = each.value.urgent.max_message_size_percent * local.sqs_message_size_max_bytes / 100

  alarm_actions = [var.urgent_sns_topic_arn]
  ok_actions    = [var.urgent_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    QueueName = each.key
  }
}

resource "aws_cloudwatch_metric_alarm" "sqs_message_too_large_casual" {
  for_each = { for alarms, alarm in local._sqs : alarms => alarm if alarm.casual.enabled && var.enable_casual }

  alarm_name        = "CAS:${local.name_prefix}${each.key}-SQS-BigMsg"
  alarm_description = "SQS queue ${each.key}: message approaching size limit"

  namespace   = "AWS/SQS"
  metric_name = "SentMessageSize"

  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  period              = "3600"
  statistic           = "Maximum"
  threshold           = each.value.casual.max_message_size_percent * local.sqs_message_size_max_bytes / 100

  alarm_actions = [var.casual_sns_topic_arn]
  ok_actions    = [var.casual_sns_topic_arn]

  treat_missing_data = "notBreaching"

  dimensions = {
    QueueName = each.key
  }
}
